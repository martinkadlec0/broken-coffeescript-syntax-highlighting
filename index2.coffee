###
  @param {number} x
  @param {number} y
###
add = (x, y)->
  x + y

###
  @param {number} x
  @param {number} y
###
subtract = (x, y)->
  x - y

###
  @param {number} x
  @param {number} y
###
multiply = (x, y)->
  x * y

###
  @param {number} x
  @param {number} y
###
divide = (x, y)->
  x / y